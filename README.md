Drunken Avenger
===============

[![Travis-CI Build Status](http://img.shields.io/travis/borntyping/rust-drunken-avenger/master.svg?style=flat-square)](https://travis-ci.org/borntyping/rust-drunken-avenger)

A [Rust]() IRC bot. In development.

Running
-------

Install `rustc` and `cargo`, then:

```bash
cargo run
```

To run with `INFO` or `DEBUG` messages:

```bash
RUST_LOG=tony=info cargo run
```

Licence
-------

Drunken Avenger is licensed under the [MIT Licence](http://opensource.org/licenses/MIT).

Author
------

Written by [Sam Clements](https://github.com/borntyping).
