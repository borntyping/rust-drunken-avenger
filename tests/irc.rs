#![feature(macro_rules)]

extern crate tony;

use tony::irc::Prefix;
use tony::irc::Message;

/// The test_parse! macro checks that Message::parse correctly sets attributes
macro_rules! test_parse(
    ($name:ident, $line:expr, $attr:ident, $expected:expr) => (
        #[test]
        fn $name() {
            assert_eq!(Message::parse($line).$attr, $expected.to_string());
        }
    );
)

// test_parse!(test_parse_prefix, ":aleph!user@example.com NICK miranda", prefix, ":aleph!user@example.com")
test_parse!(test_parse_command, ":aleph!user@example.com NICK miranda", command, "NICK")
test_parse!(test_parse_trailing, ":aleph!user@example.com NICK miranda", trailing, "miranda")

#[test]
fn test_parse_prefix() {
    assert_eq!(Prefix::parse(":aleph!user@example.com").unwrap().user, "user".to_string())
}
