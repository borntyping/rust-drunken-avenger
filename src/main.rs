extern crate tony;

#[cfg(not(test))]
fn main() {
    tony::run(("localhost", 6667));
}
