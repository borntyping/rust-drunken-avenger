use std::task::TaskBuilder;
use std::io::TcpStream;
use std::io::BufferedReader;
use std::io::BufferedWriter;
use std::io::net::ip::ToSocketAddr;

pub mod irc;

struct Client {
    pub sender: Sender<String>,
    pub receiver: Receiver<String>
}

impl Client {
    fn connect<A: ToSocketAddr>(addr: A) -> Client {
        let stream = TcpStream::connect(addr).unwrap();

        let (read_sender, read_receiver) = channel::<String>();
        let mut reader = BufferedReader::new(stream.clone());

        TaskBuilder::new().named("reader").spawn(proc() {
            loop {
                match reader.read_line() {
                    Ok(line) => {
                        print!("--> {}", line);
                        read_sender.send(line);
                    },
                    Err(_) => break
                }
            }
        });

        let (write_sender, write_receiver) = channel::<String>();
        let mut writer = BufferedWriter::new(stream.clone());

        TaskBuilder::new().named("writer").spawn(proc() {
            loop {
                match write_receiver.recv_opt() {
                    Ok(line) => {
                        print!("<-- {}\n", line);
                        writer.write_str(line.as_slice()).ok();
                    },
                    Err(e) => {
                        print!("--- Stopped writing: {}\n", e);
                        break;
                    }
                }
            }
        });

        Client{
            sender: write_sender,
            receiver: read_receiver
        }
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        self.sender.send("QUIT".to_string());
    }
}

pub fn run<A: ToSocketAddr>(addr: A) {
    let client = Client::connect(addr);

    client.sender.send("NICK :tony".to_string());
    client.sender.send("USER tony * * tony".to_string());

    loop {
        match client.receiver.recv_opt() {
            Ok(_) => (),
            Err(_) => break
        }
    }
}
