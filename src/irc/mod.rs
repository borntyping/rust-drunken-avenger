extern crate regex;

pub struct Prefix {
    pub nick: String,
    pub user: String,
    pub host: String
}

impl Prefix {
    /// Attempts to parse a prefix, e.g. ":aleph!user@example.com"
    pub fn parse(prefix: &str) -> Option<Prefix> {
        let re = match regex::Regex::new(r"^:(.+)!(.+)@(.+)$") {
            Ok(re) => re,
            Err(err) => panic!("{}", err),
        };

        match re.captures(prefix) {
            Some(caps) => Some(Prefix{
                nick: caps.at(1).to_string(),
                user: caps.at(2).to_string(),
                host: caps.at(3).to_string()
            }),
            None => None
        }
    }
}

pub struct Message {
    pub prefix: Option<Prefix>,
    pub command: String,
    pub trailing: String
}

impl Message {
    // Returns a single token from a line
    pub fn parse_token(line: &str) -> (&str, &str) {
        let tokens: Vec<&str> = line.splitn(1, ' ').collect();
        return (tokens[0], tokens[1]);
    }

    /// Parses a line and returns a Message instance
    pub fn parse(line: &str) -> Message {
        let (prefix, line) = Message::parse_token(line);
        let (command, line) = Message::parse_token(line);
        Message::new(prefix, command, line)
    }

    pub fn new(prefix: &str, command: &str, trailing: &str) -> Message {
        Message{
            prefix: Prefix::parse(prefix),
            command: command.to_string(),
            trailing: trailing.to_string()
        }
    }
}
